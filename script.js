// фунция
function createNewUser() {
    // запрашиваю инфу 
    const defaultValue = "dd.mm.yyyy";
    const firstName = prompt('Введіть ваше ім\'я');
    const lastName = prompt('Введіть ваше прізвище');
    const birthdayStr = prompt('Когда вы родились?', defaultValue );
    // соединяю даты через .
    const dateParts = birthdayStr.split('.');
    // если пользователь не введет по нормальному то выдаст ошибку 
    if (dateParts.length !== 3) {
        alert('Помилка: Ви ввели некоректну дату.');
        return null; 
    }
    // переобразую данные пользователя 
    const day = parseInt(dateParts[0]);
    const month = parseInt(dateParts[1]) - 1; // Місяць в Date починається з 0
    const year = parseInt(dateParts[2]);

    // Перевірка, чи дата коректна
    if (isNaN(day) || isNaN(month) || isNaN(year) || month < 0 || month > 11) {
        alert('Помилка: Ви ввели некоректну дату.');
        return null; 
    }
    
    const birthday = new Date(year, month, day);

    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        // Вік
        getAge: function() {
            const today = new Date();
            const age = today.getFullYear() - this.birthday.getFullYear();
            return age;
        },
        getLogin: function() {
            return firstName[0] + lastName;
        },
        // Пароль
        getPassword: function() {
            const year = this.birthday.getFullYear();
            return firstName[0] + lastName + year;
        }
    };

    return newUser;
}

const user = createNewUser();

if (user !== null) {
    console.log(user);
    const login = user.getLogin();
    console.log(login);

    // Вивід віку
    const age = user.getAge();
    console.log(`Вам ${age} лет`);

    // Вивід пароля
    const password = user.getPassword();
    console.log(`Ваш пароль: ${password}`);
}